<img src="https://develop.therapieland.nl/media/logo/logo.png" width="220" alt="">

Inspiring e-Health for a new world of care.

## API documentation
### How it works
Add API endpoints to the _specification/v1/spec.yml_ and push your changes. [Documentation](https://therapieland-public.gitlab.io/therapieland-api-documentation/) is
published here. 

If you want to see the generated documentation on your localhost:
```
yarn static-server --open
```