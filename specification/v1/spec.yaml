openapi: "3.0.3"
info:
  version: "1.0"
  title: Therapieland.nl API specification
  description: |
    OpenAPI specification for the Therapieland.nl API for integration with EPD systems.

  x-logo:
    url: ../logo.png
tags:
  - name: Authentication
    description: |
      The authentication is done by providing a Authorization header with a signed JWT Bearer token:
      ```
      Authorization: Bearer <token>
      ```
      The token is signed by a private RSA key. The public part of the RSA key-pair is configured in therapieland.nl. When
      we register the public key we will provide you with the `connector_slug` which you'll need to use for the
      issuer in the JTW token. The minimal JWT we expect contains the following fields and claims:
      ```
      {
        "header": {
          "alg": "RS256",
          "typ": "JWT"
        },
        "payload": {
          "aud": "therapieland.nl",
          "exp": 1623327090,
          "iat": 1623319890,
          "iss": "<connector_slug>>",
          "jti": "31007be0-f90f-436a-b70c-58201da8f928",
        }
      }
      ```
      To create the private-public key pair you can use the following commands:
      ```
      openssl genrsa -out jwt-key 4096
      openssl rsa -in jwt-key -pubout > jwt-key.pub
      ```
  - name: Error responses
    description: |
      Error responses are formatted as a dictionary where the key if the 'error' attribute and the value an array with key words indicating the error:
      ```
      {'external_id': ['invalid'], 'name': ['invalid']}
      ```
paths:
  /organisations:
    $ref: paths/OrganizationInstance.yaml
  /organisations/{external_id}/professionals:
    $ref: paths/ProfessionalInstance.yaml
  /organisations/{external_id}/professionals/{professional_identifier}/care-plans:
    $ref: paths/CarePlanInstance.yaml
  /organisations/{external_id}/professionals/{professional_identifier}/treatments:
    $ref: paths/ProfessionalTreatments.yaml
  /organisations/{external_id}/patients/{patient_identifier}:
    $ref: paths/PatientInvitations.yaml


